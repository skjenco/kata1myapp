package com.skjenco.myapp.repository;

import com.skjenco.myapp.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}
