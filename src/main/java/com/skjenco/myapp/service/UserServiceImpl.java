package com.skjenco.myapp.service;

import com.skjenco.myapp.model.User;
import com.skjenco.myapp.model.UserInfo;
import com.skjenco.myapp.repository.UserInfoRepository;
import com.skjenco.myapp.repository.UserRepository;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Service("userService")
public class UserServiceImpl implements  UserService {

    private static Logger logger = Logger.getLogger(UserService.class);


    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserInfoRepository userInfoRepository;


    @PersistenceContext
    EntityManager entityManager;



    @Override
    @Transactional
    public String getUserDescription() {

        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from user");

        logger.debug(list.size());

        Query query = entityManager.createQuery("select u from User u");



        List<User> lis2 = query.getResultList();

        logger.info(lis2.size());
        logger.info(lis2.get(0).getUserName());

        User user2 = userRepository.findOne(1L);

        logger.debug(user2.getUserName());


        Query query1 = entityManager.createQuery("select u from UserInfo ui left join ui.user u left join ui.address a where a.id = 1");


        List<User> list3 = query1.getResultList();

        User user4 = list3.get(0);


        //logger.debug(user4.getUserInfos().get(0).getFirstName());

        logger.debug(list3.get(0).getUserName());




        return null;
    }


    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }


    @Override
    public UserInfo getUserInfo(Long userId) {
        return userInfoRepository.getUserInfoByUserId(userId);
    }
}
