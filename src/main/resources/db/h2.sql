create table user (
  id int NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(20) NOT NULL,
  description VARCHAR(255),
  PRIMARY KEY (id)
);


create table address (
  id int NOT NULL AUTO_INCREMENT,
  street VARCHAR(50),
  city  VARCHAR(30),
  state VARCHAR(20),
  zip VARCHAR(10),
  PRIMARY KEY (id)
);


create table user_info (
  id int NOT NULL AUTO_INCREMENT,
  user_id int NOT NULL,
  address_id int,
  frist_name VARCHAR(20),
  last_name VARCHAR(20),
  description VARCHAR(255),
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (address_id) REFERENCES address(id)
);

insert into user (user_name, description) VALUES ('skjenco','skejenco is a user');
INSERT INTO user (user_name, description) VALUES ('bruce', 'dont want to say whjat I think');
insert into address (street, city, state, zip) VALUES ('3270 Lee May','granger','utah','84119');
insert into address (street, city, state, zip) VALUES ('2818 Whitehall','west valle','utah','84119');
insert INTO user_info (user_id, address_id, frist_name, last_name, description) VALUES
  (1,2,'Stephen','jensen','just a user');

